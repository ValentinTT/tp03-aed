from random import randint, choice
import registro
import uuid

"""1)    Nuevos por precio: generar un nuevo arreglo conteniendo sólo las publicaciones de artículos nuevos. Mostrar el listado ordenado por precio (de menor a mayor).
2)    Usados por calificación: determinar cantidad de publicaciones en estado usado por cada puntuación de vendedor (5 totales).
3)    Distribución geográfica: informar la cantidad de artículos disponibles por ubicación geográfica y puntuación del vendedor, pero mostrando el resultado en forma de matriz. Se debe mostrar el nombre de cada provincia (no el código) y la descripción de la puntuación del vendedor (no el código), y solo para las cantidades mayores a cero.
4)    Total provincial: a partir de la matriz, informar el total de artículos para una provincia que se ingresa por teclado (validar que se encuentre cargada).
5)    Precio promedio de usados: mostrar las publicaciones con estado usado, cuyo precio supera el precio promedio de usados del listado.
6)    Compra ideal: informar cual es el menor precio para un artículo nuevo, omitiendo los vendedores con calificación Mala.
7)    Comprar: buscar una publicación cuyo código se ingresa por teclado. Si no existe, informar con un mensaje. Si existe, preguntar al usuario qué cantidad de artículos desea comprar, validar que la cantidad disponible sea suficiente, y confirmar/rechazar la compra según corresponda."""

#TODO: Documentar
#TODO: Dudas
#TODO: Podemos definir funciones dentro del registro o no?
"""Dudas. 
1-El punto tres, que hay que hacer?
2-El codigo de publicacion como se genera? formato.
3-Al confirmar la compra le descuento al producto la cantidad?
"""

def int_input(msj):
    n = ""
    while not n.isdigit():
        n = input(msj)
    return int(n)

def crear_resultado():
    #TODO: Definir bien el formato del codigo. por ahora va a ser un uuid v1 convertido a string
    cod_pub = uuid.uuid1()
    precio = randint(200, 2000)
    ubicacion = randint(1, len(registro.provincias))
    estado = choice(registro.estados)
    cant = randint(0, 10)
    puntuacion = randint(1, len(registro.puntuaciones))
    producto = registro.Producto(str(cod_pub), precio, ubicacion, estado, cant, puntuacion)
    return producto

def generar_resultados():
    n = int_input("Ingrese la cantidad n de resultados deseados: ")
    arr_res = [None] * n
    for i in range(n):
        arr_res[i] = crear_resultado()
    ordenar_por_cod_pub(arr_res)
    return arr_res

def ordenar_por_cod_pub(arr):
    #TODO: Cambiar el algoritmo de ordenacion por uno mejor.
    #TODO: No dice si ordenar de menor a mayor o al revés.
    n = len(arr)
    for i in range(n-1):
        for j in range(i+1, n):
            if arr[i].codigo_publicacion > arr[j].codigo_publicacion:
                arr[i], arr[j] = arr[j], arr[i]

def calcular_promedio_usados(arr):
    tot = 0
    cont_usados = 0
    for i in range(len(arr)):
        if arr[i].estado == registro.estados[1]:
            tot += arr[i].precio
            cont_usados += 1
    return round(tot/cont_usados, 2)

def usados_sobre_precio(arr, precio):
    for i in arr:
        if i.precio >= precio:
            i.write_product()

def menor_precio_art_nuevo(arr):
    index = -1
    for i in range(len(arr)): #Busca el primer elemento nuevo sin reputacion mala
        if arr[i].estado == registro.estados[0] and arr[i].puntuacion != registro.puntuaciones[0]:
            index = i
            break
    if index < 0: #No hay ningun articulo que cumpla con la condicion de nuevo y reputacion distinta de mala
        return index

    for i in range(index, len(arr)):
        if arr[i].estado == registro.estados[1] or arr[i].puntuacion == registro.puntuaciones[0]: #articulo o usado o con reputacion mala
            continue
        if arr[i].precio < arr[index].precio:
            index = i
    return index

def indice_producto_por_codigo(arr, cod_pub):
    for i in range(len(arr)):
        if arr[i].codigo_publicacion == cod_pub:
            return i
    return -1

def main():
    arr_resultados = generar_resultados()
    for i in arr_resultados:
        i.write_product()

    op = 0
    while op != 8:
        print("*"*80, "\nMenu de opciones:\n"\
              "1 \t Nuevos por precio.\n"\
              "2 \t Usados por calificación.\n"\
              "3 \t Distribucion geográfica.\n"\
              "4 \t Total provincial.\n"\
              "5 \t Precio promedio de usados.\n"\
              "6 \t Compra ideal.\n"\
              "7 \t Comprar.\n"\
              "8 \t Salir.")
        op = int_input("Ingrese una opción: ")
        #TODO: Completar incisos 1-4
        if op == 1:
            pass
        elif op == 2:
            pass
        elif op == 3:
            pass
        elif op == 4:
            pass
        elif op == 5:
            prom = calcular_promedio_usados(arr_resultados)
            print("El precio promedio de artículos usados es {}$.\nLos articulos sobre este precio son:".format(prom))
            usados_sobre_precio(arr_resultados, prom)
        elif op == 6:
            index = menor_precio_art_nuevo(arr_resultados)
            if index < 0:
                print("No hay articulos usados con puntuación distinta de 'Mala'.")
            else:
                print("El mejor articulo nuevo es:")
                arr_resultados[index].write_product()
        elif op == 7:
            index = -1
            while index == -1:
                cod_pub = input("Ingrese el código de la publicación que desea comprar: ")
                index = indice_producto_por_codigo(arr_resultados, cod_pub)
                if index == -1:
                    print("El codigo ingresado no corresponde a ningun artículo.")
            cant_max = arr_resultados[index].cant_disponible
            cant = 0
            while cant < 1 or cant > cant_max:
                cant = int_input("Ingrese la cantidad de unidades que desa comprar (Maximo {}): ".format(cant_max))
                if cant < 1 or cant > cant_max:
                    print("Recuerde que debe ingresar un número entre 1 y {}".format(cant_max))
            #TODO: Descontar producto
        elif op == 8:
            break
        else:
            print("La opción debe estar entre 1 y 8!!!")

if __name__ == "__main__":
    main()